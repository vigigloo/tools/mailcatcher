FROM ruby:alpine

RUN apk add --no-cache ca-certificates openssl sqlite-libs libstdc++

ARG MAILCATCHER_VERSION=0.8.0

RUN apk add --no-cache --virtual .build-deps ruby-dev make g++ sqlite-dev
RUN gem install --no-document -v $MAILCATCHER_VERSION mailcatcher
RUN apk del .build-deps

RUN adduser -D mailcatcher

USER mailcatcher

EXPOSE 1025 1080

CMD ["mailcatcher", "--foreground", "--ip=0.0.0.0"]
