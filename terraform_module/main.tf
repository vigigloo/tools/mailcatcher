resource "helm_release" "mailcatcher" {
  chart           = "mailcatcher"
  repository      = "https://gitlab.com/api/v4/projects/34260905/packages/helm/stable"
  name            = var.chart_name
  namespace       = var.namespace
  version         = var.chart_version
  force_update    = var.helm_force_update
  recreate_pods   = var.helm_recreate_pods
  cleanup_on_fail = var.helm_cleanup_on_fail
  max_history     = var.helm_max_history

  values = var.values

  dynamic "set" {
    for_each = var.image_repository == null ? [] : [var.image_repository]
    content {
      name  = "image.repository"
      value = var.image_repository
    }
  }

  dynamic "set" {
    for_each = var.image_tag == null ? [] : [var.image_tag]
    content {
      name  = "image.tag"
      value = var.image_tag
    }
  }

  dynamic "set" {
    for_each = var.limits_cpu == null ? [] : [var.limits_cpu]
    content {
      name  = "resources.limits.cpu"
      value = var.limits_cpu
    }
  }

  dynamic "set" {
    for_each = var.limits_memory == null ? [] : [var.limits_memory]
    content {
      name  = "resources.limits.memory"
      value = var.limits_memory
    }
  }

  dynamic "set" {
    for_each = var.requests_cpu == null ? [] : [var.requests_cpu]
    content {
      name  = "resources.requests.cpu"
      value = var.requests_cpu
    }
  }

  dynamic "set" {
    for_each = var.requests_memory == null ? [] : [var.requests_memory]
    content {
      name  = "resources.requests.memory"
      value = var.requests_memory
    }
  }

  dynamic "set" {
    for_each = var.ingress == null ? [] : [var.ingress]
    content {
      name  = "ingress.enabled"
      value = tostring(var.ingress)
    }
  }

  dynamic "set" {
    for_each = var.ingress_tls == null ? [] : [var.ingress_tls]
    content {
      name  = "ingress.tls.enabled"
      value = tostring(var.ingress_tls)
    }
  }

  dynamic "set" {
    for_each = var.ingress_hosts
    content {
      name  = "ingress.hosts.${set.key}.host"
      value = set.value
    }
  }
}
