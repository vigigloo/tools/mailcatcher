output "domain" {
  value      = "${var.chart_name}.${var.namespace}.svc.cluster.local"
  depends_on = [helm_release.mailcatcher]
}
